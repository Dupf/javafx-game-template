package de.awacademy;

import de.awacademy.model.Model;
import de.awacademy.model.Stein;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class Graphics {

    private GraphicsContext gc;
    private Model model;
    private Image ende1;
    private Image ende2;

    public Graphics (GraphicsContext gc, Model model) {
        this.gc = gc;
        this.model = model;
        try {
            ende1 = new Image(new FileInputStream("src/de/awacademy/bilder/Spieler1gewinnt.png"));
            ende2 = new Image(new FileInputStream("src/de/awacademy/bilder/Spieler2gewinnt.png"));
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }


    public void draw() throws FileNotFoundException {
        gc.clearRect(0, 0, model.WIDTH, model.HEIGHT);

        // Hier wird das Brett gezeichnet
        gc.strokeLine(125,425,325,425);
        gc.strokeLine(525,425,725,425);
        gc.strokeLine(425,125,425,325);
        gc.strokeLine(425,525,425,725);
        gc.strokeRect(125,125,600,600);
        gc.strokeRect(225,225,400,400);
        gc.strokeRect(325,325,200,200);

        //Hier wird der Waehler eingefärbt und bewegt
        if (model.isWeiß()) {
            gc.setFill(Color.rgb(0, 255, 0, 0.5));
            gc.fillRect(model.getWaehler().getX(), model.getWaehler().getY(), model.getWaehler().getWidth(), model.getWaehler().getHight());
        } else {
            gc.setFill(Color.rgb(255, 0, 0, 0.5));
            gc.fillRect(model.getWaehler().getX(), model.getWaehler().getY(), model.getWaehler().getWidth(), model.getWaehler().getHight());
        }
        //Steine für schwarz und weiß neben dem Spielfeld platzieren
        gc.setFill(Color.rgb(0,255,0));
        gc.setStroke(Color.rgb(0,0,0));
        for (Stein stein:model.getPlayer1().getSteine()) {
            gc.fillOval(stein.getX(),
                    stein.getY(),
                    stein.getWidth(),
                    stein.getHight());
        }
        gc.setFill(Color.rgb(255,0,0));
        gc.setStroke(Color.rgb(0,0,0));
        for (Stein stein:model.getPlayer2().getSteine()) {
            gc.fillOval(stein.getX(),
                    stein.getY(),
                    stein.getWidth(),
                    stein.getHight());
        }


        ////ENDE//////
        gc.drawImage(ende1,0,0,model.getEnde1(),model.getEnde1());
        gc.drawImage(ende2,0,0,model.getEnde2(),model.getEnde2());







        /*gc.setFill(Color.YELLOW);
        gc.fillOval(40, 0.0,
                200, 100
        );*/

    }

}
