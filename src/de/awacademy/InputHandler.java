package de.awacademy;

import de.awacademy.model.Model;
import javafx.application.Platform;
import javafx.scene.input.KeyCode;

public class InputHandler {

    private Model model;

    public InputHandler(Model model) {
        this.model = model;
    }

    public void onKeyPressed(KeyCode keycode) {

        if (keycode == KeyCode.UP) {
            model.moveUp();
        }
        if (keycode == KeyCode.DOWN) {
            model.moveDown();
        }
        if (keycode == KeyCode.LEFT) {
            model.moveLeft();
        }
        if (keycode == KeyCode.RIGHT) {
            model.moveRight();
        }

        if (keycode == KeyCode.ENTER) {
            // erste Phase (Steine setzen) --- 9 Züge hat jeder Spieler um seine Steine zu setzen
            model.steinSetzen();
            model.ende();

        }

        if (keycode == KeyCode.ESCAPE){
            Platform.exit();
        }
        if (keycode == KeyCode.SPACE){
            model.restart();
        }

    }
}

