package de.awacademy.model;


import java.util.List;

public class Waehler {
    private int x;
    private int y;
    private int hight;
    private int width;
    private int tempIndex;



    public Waehler (){
        x = 100;
        y = 100;
        hight = 50;
        width = 50;
    }

    public void move(int dx, int dy){
        x += dx;
        y += dy;
    }

    public boolean auswaehlen(List<Stein> steine){
        for (int i = 0; i < 9; i++){
            if (this.getX() == steine.get(i).getX()   && this.getY() == steine.get(i).getY()){
                tempIndex = i;
                return true;
            }
        }
        return false;
    }



    public boolean aufSteinpruefen(){

        return true;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getHight() {
        return hight;
    }

    public void setHight(int hight) {
        this.hight = hight;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getTempIndex() {
        return tempIndex;
    }
}
