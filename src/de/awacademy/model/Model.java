package de.awacademy.model;

import java.util.List;

public class Model {
    private Player player1;
    private Player player2;
    private Waehler waehler;
    private int entfernteSteine1;
    private int entfernteSteine2;
    private int ende1 = 0;
    private int ende2 = 0;
    private boolean waehlbar;
    private boolean platzierbar;
    private boolean weiß;


    public final double WIDTH = 825;
    public final double HEIGHT = 825;

    public Model() {
        this.waehler = new Waehler();
        player1 = new Player(true);
        player2 = new Player (false);
        entfernteSteine1 = player1.getEntfernteSteine();
        entfernteSteine2 = player2.getEntfernteSteine();
        ende1 = 0;
        ende2 = 0;
        waehlbar = true;
        platzierbar = true;
        weiß = true;
    }

    public void moveUp() {
        if (waehler.getY() > 100 && (waehler.getX() == 100 || waehler.getX() == 700)) {
            waehler.move(0, -300);
        } else if (waehler.getY() > 200 && (waehler.getX() == 200 || waehler.getX() == 600)) {
            waehler.move(0, -200);
        } else if (((waehler.getY() >= 200 && waehler.getY() <= 300) ||
                (waehler.getY() >= 600 && waehler.getY() <= 700))
                && waehler.getX() > 300 && waehler.getX() < 500) {
            waehler.move(0, -100);
        } else if (((waehler.getY() >= 400 && waehler.getY() <= 500)
                && (waehler.getX() == 300 || waehler.getX() == 500))) {
            waehler.move(0, -100);
        }
        setPlatzierbar(true);
    }

    public void moveDown() {
        if (waehler.getY() != 700 && (waehler.getX() == 100 || waehler.getX() == 700)) {
            waehler.move(0, 300);
        } else if (waehler.getY() < 600 && (waehler.getX() == 200 || waehler.getX() == 600)) {
            waehler.move(0, 200);
        } else if (((waehler.getY() >= 100 && waehler.getY() <= 200) ||
                (waehler.getY() >= 500 && waehler.getY() <= 600))
                && waehler.getX() > 300 && waehler.getX() < 500) {
            waehler.move(0, 100);
        } else if (((waehler.getY() <= 400 && waehler.getY() >= 300)
                && (waehler.getX() == 300 || waehler.getX() == 500))) {
            waehler.move(0, 100);
        }
        setPlatzierbar(true);
    }

    public void moveLeft() {
        if (waehler.getX() > 100 && (waehler.getY() == 100 || waehler.getY() == 700)) {
            waehler.move(-300, 0);
        } else if (waehler.getX() >= 400 && (waehler.getY() == 200 || waehler.getY() == 600)) {
            waehler.move(-200, 0);
        } else if (((waehler.getX() >= 200 && waehler.getX() <= 300) ||
                (waehler.getX() >= 600 && waehler.getX() <= 700))
                && waehler.getY() == 400) {
            waehler.move(-100, 0);
        } else if (((waehler.getX() >= 400)
                && (waehler.getY() == 300 || waehler.getY() == 500))) {
            waehler.move(-100, 0);
        }
        setPlatzierbar(true);
    }

    public void moveRight() {
        if (waehler.getX() < 700 && (waehler.getY() == 100 || waehler.getY() == 700)) {
            waehler.move(300, 0);
        } else if (waehler.getX() <= 400 && (waehler.getY() == 200 || waehler.getY() == 600)) {
            waehler.move(200, 0);
        } else if (((waehler.getX() <= 200) ||
                (waehler.getX() >= 500 && waehler.getX() <= 600))
                && waehler.getY() == 400) {
            waehler.move(100, 0);
        } else if (((waehler.getX() <= 400)
                && (waehler.getY() == 300 || waehler.getY() == 500))) {
            waehler.move(100, 0);
        }
        setPlatzierbar(true);
    }

    //////////////////////////////////////////////////////////ENDE DER BEWEGUNGSLOGIK DES WAEHLERS //////////////////////////
    public void steinSetzen() {

        if (player1.isMuehle() && !isPlatzierbarMuehle(player2)) {    // Abfrage ob eine Mühle besteht und ob man einen Stein ausgewählt hat

            player1.steinEntfernen(player2, waehler, false);

            player1.setMuehle(false);
            this.setWeiß(false);

            waehlbar = true;
        } else if (player2.isMuehle() && !isPlatzierbarMuehle(player1)) {  // Abfrage ob eine Mühle besteht und ob man einen Stein ausgewählt hat
            player2.setMuehle(false);
            player1.steinEntfernen(player1, waehler, true);
            this.setWeiß(true);

            waehlbar = true;
        } else if (player1.getVerbleibendeSteine() == 3 && weiß && !waehlbar) {
            player1.getSteine().get(waehler.getTempIndex()).setX(waehler.getX());
            player1.getSteine().get(waehler.getTempIndex()).setY(waehler.getY());
            // Auf Muehle pruefen
            if (player1.aufMuehlePruefen(waehler)) {

            } else {

                waehlbar = true;
                weiß = false;
            }
        } else if (player2.getVerbleibendeSteine() == 3 && !weiß && !waehlbar) {
            player2.getSteine().get(waehler.getTempIndex()).setX(waehler.getX());
            player2.getSteine().get(waehler.getTempIndex()).setY(waehler.getY());
            // Auf Muehle pruefen
            if (player2.aufMuehlePruefen(waehler)) {

            } else {

                waehlbar = true;
                weiß = true;
            }
        } else if (player1.getZug() < 9) {  // Abfrage ob es sich um die ersten 9 Züge handelt
            if (weiß && isPlatzierbar()) {   // Abfrage ob der Stein setzbar ist
                player1.getSteine().get(player1.getZug()).setX(waehler.getX());
                player1.getSteine().get(player1.getZug()).setY(waehler.getY());
                // Größe der Steine anpassen
                player1.getSteine().get(player1.getZug()).setWidth(50);
                player1.getSteine().get(player1.getZug()).setHight(50);
                // Auf Mühle prüfen und entsprechend reagieren
                if (player1.aufMuehlePruefen(waehler)) {

                } else {
                    this.setWeiß(false);

                }


            } else if (isPlatzierbar()) {
                player2.getSteine().get(player2.getZug()).setX(waehler.getX());
                player2.getSteine().get(player2.getZug()).setY(waehler.getY());
                // Größe der Steine anpassen
                player2.getSteine().get(player2.getZug()).setWidth(50);
                player2.getSteine().get(player2.getZug()).setHight(50);
                // Auf Muehle pruefen
                if (player2.aufMuehlePruefen(waehler)) {

                } else {
                    this.setWeiß(true);

                }
                // zug variable in beiden Player Objekten hochsetzen
                player1.setZug(+1);
                player2.setZug(+1);


            }
        } else if (weiß && waehlbar) {
            if (waehler.auswaehlen(player1.getSteine())) {
                waehlbar = false;
            } else waehlbar = true;
        } else if (!weiß && waehlbar) {
            if (waehler.auswaehlen(player2.getSteine())) {
                waehlbar = false;
            } else waehlbar = true;
        } else if (weiß && !waehlbar && player1.isBewegbar(waehler) && isPlatzierbar()) {
            player1.getSteine().get(waehler.getTempIndex()).setX(waehler.getX());
            player1.getSteine().get(waehler.getTempIndex()).setY(waehler.getY());
            // Auf Muehle pruefen
            if (player1.aufMuehlePruefen(waehler)) {

            } else {

                waehlbar = true;
                weiß = false;
            }

        } else if (!weiß && !waehlbar && player2.isBewegbar(waehler) && isPlatzierbar()) {
            player2.getSteine().get(waehler.getTempIndex()).setX(waehler.getX());
            player2.getSteine().get(waehler.getTempIndex()).setY(waehler.getY());
            // Auf Muehle pruefen
            if (player2.aufMuehlePruefen(waehler)) {

            } else {

                waehlbar = true;
                weiß = true;
            }

        }

    }

    public boolean isPlatzierbar() {
        for (Stein stein : player1.getSteine()) {
            if (stein.getX() == waehler.getX() && stein.getY() == waehler.getY()) {
                platzierbar = false;
            }
        }
        for (Stein stein : player2.getSteine()) {
            if (stein.getX() == waehler.getX() && stein.getY() == waehler.getY()) {
                platzierbar = false;
            }
        }

        return platzierbar;
    }

    public boolean isPlatzierbarMuehle(Player player) {
        for (Stein stein : player.getSteine()) {
            if (stein.getX() == waehler.getX() && stein.getY() == waehler.getY()) {
                platzierbar = false;
            }
        }
        return platzierbar;
    }

    public int getEnde1() {
        return ende1;
    }

    public void setEnde1(int ende1) {
        this.ende1 = ende1;
    }

    public int getEnde2() {
        return ende2;
    }

    public void setEnde2(int ende2) {
        this.ende2 = ende2;
    }

    public boolean ende() {
        if (player1.getVerbleibendeSteine() < 3) {
            setEnde2(800);
            return true;
        } else if (player2.getVerbleibendeSteine() < 3) {
            setEnde1(800);
            return true;
        }
        return false;
    }

    public void restart(){
        this.waehler = new Waehler();
        player1 = new Player(true);
        player2 = new Player (false);
        entfernteSteine1 = player1.getEntfernteSteine();
        entfernteSteine2 = player2.getEntfernteSteine();
        ende1 = 0;
        ende2 = 0;
        waehlbar = true;
        platzierbar = true;
        weiß = true;
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////


    public int getEntfernteSteine1() {
        return entfernteSteine1;
    }

    public void setEntfernteSteine1(int entfernteSteine) {
        this.entfernteSteine1 += entfernteSteine;
    }

    public int getEntfernteSteine2() {
        return entfernteSteine2;
    }

    public void setEntfernteSteine2(int entfernteSteine) {
        this.entfernteSteine2 += entfernteSteine;
    }

    public Waehler getWaehler() {
        return waehler;
    }


    public Player getPlayer1() {
        return player1;
    }

    public Player getPlayer2() {
        return player2;
    }

    public void update(long nowNano) {

        //player.move(, 0);
    }

    public boolean isWeiß() {
        return weiß;
    }

    public void setWeiß(boolean weiß) {
        this.weiß = weiß;
    }


    public void setPlatzierbar(boolean platzierbar) {
        this.platzierbar = platzierbar;
    }
}
