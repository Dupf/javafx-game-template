package de.awacademy.model;

import java.util.ArrayList;

public class Player {
    private int zug = 0;
    private int verbleibendeSteine = 9;
    private int zaehlerFuerMuehle = 0;
    private int entfernteSteine = 0;
    private ArrayList<Stein> steine = new ArrayList<>(0);
    private boolean weiß;
    private boolean bewegbar;
    private boolean muehle;


    public Player(boolean weiß) {
        this.weiß = weiß;
        steine = getSteinReihe();
    }


    public ArrayList<Stein> getSteinReihe() {
        if (weiß) {
            for (int i = 0; i < 9; i++) {
                steine.add(new Stein(weiß, 50, (i * 20) + 175));
            }
        } else {
            for (int i = 0; i < 9; i++) {
                steine.add(new Stein(weiß, 50, (i * 20) + 475));
            }
        }
        return steine;
    }

    public boolean aufMuehlePruefen(Waehler waehler) {
        if (zaehlerFuerMuehle != 3) {
            // Pruefen ob eine vertikale Muehle auf den aeusseren Vierecken möglich ist
            getSteine().stream()
                    .filter(t -> (t.getX() <= 300 || t.getX() >= 500))
                    .filter(t -> (t.getX() == waehler.getX()))
                    .forEach(t -> ++zaehlerFuerMuehle);
            if (zaehlerFuerMuehle < 3) zaehlerFuerMuehle = 0;
        }
        if (zaehlerFuerMuehle != 3 && waehler.getY() <= 300) {
            // Pruefen ob eine vertikale Muehle auf der zentralen Y-Achse oben möglich ist möglich ist //////////WIP
            getSteine().stream()
                    .filter(t -> (t.getX() == 400) && t.getY() <= 300)
                    .filter(t -> (t.getX() == waehler.getX()))
                    .forEach(t -> ++zaehlerFuerMuehle);
            if (zaehlerFuerMuehle < 3) zaehlerFuerMuehle = 0;
        }
        if (zaehlerFuerMuehle != 3 && waehler.getY() >= 500) {
            // Pruefen ob eine vertikale Muehle auf der zentralen Y-Achse unten möglich ist möglich ist //////////WIP
            getSteine().stream()
                    .filter(t -> (t.getX() == 400) && t.getY() >= 500)
                    .filter(t -> (t.getX() == waehler.getX()))
                    .forEach(t -> ++zaehlerFuerMuehle);
            if (zaehlerFuerMuehle < 3) zaehlerFuerMuehle = 0;
        }
        if (zaehlerFuerMuehle != 3) {
            // Pruefen ob eine horizontale Muehle auf den ausseren Vierecken möglich ist
            getSteine().stream()
                    .filter(t -> (t.getY() <= 300 || t.getY() >= 500))
                    .filter(t -> (t.getY() == waehler.getY()))
                    .forEach(t -> ++zaehlerFuerMuehle);
            if (zaehlerFuerMuehle < 3) zaehlerFuerMuehle = 0;
        }
        if (zaehlerFuerMuehle != 3 && waehler.getX() <= 300) {
            // Pruefen ob eine vertikale Muehle auf der zentralen X-Achse links möglich ist möglich ist //////////WIP
            getSteine().stream()
                    .filter(t -> (t.getY() == 400) && t.getX() <= 300)
                    .filter(t -> (t.getY() == waehler.getY()))
                    .forEach(t -> ++zaehlerFuerMuehle);
            if (zaehlerFuerMuehle < 3) zaehlerFuerMuehle = 0;
        }
        if (zaehlerFuerMuehle != 3 && waehler.getX() >= 500) {
            // Pruefen ob eine vertikale Muehle auf der zentralen Y-Achse unten möglich ist möglich ist //////////WIP
            getSteine().stream()
                    .filter(t -> (t.getY() == 400) && t.getX() >= 500)
                    .filter(t -> (t.getY() == waehler.getY()))
                    .forEach(t -> ++zaehlerFuerMuehle);
            if (zaehlerFuerMuehle < 3) zaehlerFuerMuehle = 0;
        }
        if (zaehlerFuerMuehle == 3) {
            zaehlerFuerMuehleZuruecksetzen();
            muehle = true;
            return true;
        } else zaehlerFuerMuehleZuruecksetzen();

        return false;
    }

    public int steinEntfernen(Player player, Waehler waehler ,boolean weiß) {

        for (Stein stein : player.getSteine()) {
            if (stein.getX() == waehler.getX() && stein.getY() == waehler.getY()) {
                if (weiß) {
                    stein.setX(750);
                    stein.setY((player.getEntfernteSteine() * 20) + 175);
                    stein.setHight(15);
                    stein.setWidth(15);
                    player.setVerbleibendeSteine(1);
                    player.setEntfernteSteine(1);
                } else {
                    stein.setX(750);
                    stein.setY((player.getEntfernteSteine() * 20) + 475);
                    stein.setHight(15);
                    stein.setWidth(15);
                    player.setVerbleibendeSteine(1);
                    player.setEntfernteSteine(1);
                }
            }
        }
        return entfernteSteine + 1;
    }


    public boolean isBewegbar(Waehler waehler) {
        // Methode zum Prüfen der Bewegbarkeit eines Steins (nach den ersten 9 Zügen)
        //Abfrage für alle möglichen Steine auf dem Feld
        // Stein bei 100,100
        if ((getSteine().get(waehler.getTempIndex()).getX() == 100
                && getSteine().get(waehler.getTempIndex()).getY() == 100)
                && ((waehler.getX() == 100 && waehler.getY() == 400)
                || (waehler.getX() == 400 && waehler.getY() == 100))) {
            return !bewegbar;
        } // Stein bei 400,100
        else if ((getSteine().get(waehler.getTempIndex()).getX() == 400
                && getSteine().get(waehler.getTempIndex()).getY() == 100)
                && ((waehler.getX() == 100 && waehler.getY() == 100)
                || (waehler.getX() == 700 && waehler.getY() == 100)
                || (waehler.getX() == 400 && waehler.getY() == 200))) {
            return !bewegbar;
        } // Stein bei 700,100
        else if ((getSteine().get(waehler.getTempIndex()).getX() == 700
                && getSteine().get(waehler.getTempIndex()).getY() == 100)
                && ((waehler.getX() == 400 && waehler.getY() == 100)
                || (waehler.getX() == 700 && waehler.getY() == 400))) {
            return !bewegbar;
        } // Stein bei 700,400
        else if ((getSteine().get(waehler.getTempIndex()).getX() == 700
                && getSteine().get(waehler.getTempIndex()).getY() == 400)
                && ((waehler.getX() == 700 && waehler.getY() == 100)
                || (waehler.getX() == 700 && waehler.getY() == 700)
                || (waehler.getX() == 600 && waehler.getY() == 400))) {
            return !bewegbar;
        } // Stein bei 700,700
        else if ((getSteine().get(waehler.getTempIndex()).getX() == 700
                && getSteine().get(waehler.getTempIndex()).getY() == 700)
                && ((waehler.getX() == 700 && waehler.getY() == 400)
                || (waehler.getX() == 400 && waehler.getY() == 700))) {
            return !bewegbar;
        } // Stein bei 400,700
        else if ((getSteine().get(waehler.getTempIndex()).getX() == 400
                && getSteine().get(waehler.getTempIndex()).getY() == 700)
                && ((waehler.getX() == 100 && waehler.getY() == 700)
                || (waehler.getX() == 700 && waehler.getY() == 700)
                || (waehler.getX() == 400 && waehler.getY() == 600))) {
            return !bewegbar;
        } // Stein bei 700,700
        else if ((getSteine().get(waehler.getTempIndex()).getX() == 100
                && getSteine().get(waehler.getTempIndex()).getY() == 700)
                && ((waehler.getX() == 100 && waehler.getY() == 400)
                || (waehler.getX() == 400 && waehler.getY() == 700))) {
            return !bewegbar;
        } // Stein bei 400,700
        else if ((getSteine().get(waehler.getTempIndex()).getX() == 100
                && getSteine().get(waehler.getTempIndex()).getY() == 400)
                && ((waehler.getX() == 100 && waehler.getY() == 100)
                || (waehler.getX() == 100 && waehler.getY() == 700)
                || (waehler.getX() == 200 && waehler.getY() == 400))) {
            return !bewegbar;
        } ///////////////////////////ENDE GROSSES VIERECK/////////////////////////////////
        // Stein 200,200
        if ((getSteine().get(waehler.getTempIndex()).getX() == 200
                && getSteine().get(waehler.getTempIndex()).getY() == 200)
                && ((waehler.getX() == 200 && waehler.getY() == 400)
                || (waehler.getX() == 400 && waehler.getY() == 200))) {
            return !bewegbar;
        } // Stein bei 400,200
        else if ((getSteine().get(waehler.getTempIndex()).getX() == 400
                && getSteine().get(waehler.getTempIndex()).getY() == 200)
                && ((waehler.getX() == 200 && waehler.getY() == 200)
                || (waehler.getX() == 600 && waehler.getY() == 200)
                || (waehler.getX() == 400 && waehler.getY() == 300)
                || (waehler.getX() == 400 && waehler.getY() == 100))) {
            return !bewegbar;
        } // Stein bei 600,200
        else if ((getSteine().get(waehler.getTempIndex()).getX() == 600
                && getSteine().get(waehler.getTempIndex()).getY() == 200)
                && ((waehler.getX() == 400 && waehler.getY() == 200)
                || (waehler.getX() == 600 && waehler.getY() == 400))) {
            return !bewegbar;
        } // Stein bei 600,400
        else if ((getSteine().get(waehler.getTempIndex()).getX() == 600
                && getSteine().get(waehler.getTempIndex()).getY() == 400)
                && ((waehler.getX() == 600 && waehler.getY() == 200)
                || (waehler.getX() == 600 && waehler.getY() == 600)
                || (waehler.getX() == 500 && waehler.getY() == 400)
                || (waehler.getX() == 700 && waehler.getY() == 400))) {
            return !bewegbar;
        } // Stein bei 600,600
        else if ((getSteine().get(waehler.getTempIndex()).getX() == 600
                && getSteine().get(waehler.getTempIndex()).getY() == 600)
                && ((waehler.getX() == 600 && waehler.getY() == 400)
                || (waehler.getX() == 400 && waehler.getY() == 600))) {
            return !bewegbar;
        } // Stein bei 400,600
        else if ((getSteine().get(waehler.getTempIndex()).getX() == 400
                && getSteine().get(waehler.getTempIndex()).getY() == 600)
                && ((waehler.getX() == 200 && waehler.getY() == 600)
                || (waehler.getX() == 600 && waehler.getY() == 600)
                || (waehler.getX() == 400 && waehler.getY() == 700)
                || (waehler.getX() == 400 && waehler.getY() == 500))) {
            return !bewegbar;
        } // Stein bei 200,600
        else if ((getSteine().get(waehler.getTempIndex()).getX() == 200
                && getSteine().get(waehler.getTempIndex()).getY() == 600)
                && ((waehler.getX() == 200 && waehler.getY() == 400)
                || (waehler.getX() == 400 && waehler.getY() == 600))) {
            return !bewegbar;
        } // Stein bei 200,400
        else if ((getSteine().get(waehler.getTempIndex()).getX() == 200
                && getSteine().get(waehler.getTempIndex()).getY() == 400)
                && ((waehler.getX() == 200 && waehler.getY() == 200)
                || (waehler.getX() == 200 && waehler.getY() == 600)
                || (waehler.getX() == 300 && waehler.getY() == 400)
                || (waehler.getX() == 100 && waehler.getY() == 400))) {
            return !bewegbar;
        } ///////////////////////////ENDE MITTLERES VIERECK/////////////////////////////////
        // Stein 300,300
        if ((getSteine().get(waehler.getTempIndex()).getX() == 300
                && getSteine().get(waehler.getTempIndex()).getY() == 300)
                && ((waehler.getX() == 300 && waehler.getY() == 400)
                || (waehler.getX() == 400 && waehler.getY() == 300))) {
            return !bewegbar;
        } // Stein bei 400,300
        else if ((getSteine().get(waehler.getTempIndex()).getX() == 400
                && getSteine().get(waehler.getTempIndex()).getY() == 300)
                && ((waehler.getX() == 300 && waehler.getY() == 300)
                || (waehler.getX() == 500 && waehler.getY() == 300)
                || (waehler.getX() == 400 && waehler.getY() == 200))) {
            return !bewegbar;
        } // Stein bei 500,300
        else if ((getSteine().get(waehler.getTempIndex()).getX() == 500
                && getSteine().get(waehler.getTempIndex()).getY() == 300)
                && ((waehler.getX() == 400 && waehler.getY() == 300)
                || (waehler.getX() == 500 && waehler.getY() == 400))) {
            return !bewegbar;
        } // Stein bei 500,400
        else if ((getSteine().get(waehler.getTempIndex()).getX() == 500
                && getSteine().get(waehler.getTempIndex()).getY() == 400)
                && ((waehler.getX() == 500 && waehler.getY() == 300)
                || (waehler.getX() == 600 && waehler.getY() == 400)
                || (waehler.getX() == 500 && waehler.getY() == 500))) {
            return !bewegbar;
        } // Stein bei 500,500
        else if ((getSteine().get(waehler.getTempIndex()).getX() == 500
                && getSteine().get(waehler.getTempIndex()).getY() == 500)
                && ((waehler.getX() == 500 && waehler.getY() == 400)
                || (waehler.getX() == 400 && waehler.getY() == 500))) {
            return !bewegbar;
        } // Stein bei 400,500
        else if ((getSteine().get(waehler.getTempIndex()).getX() == 400
                && getSteine().get(waehler.getTempIndex()).getY() == 500)
                && ((waehler.getX() == 300 && waehler.getY() == 500)
                || (waehler.getX() == 500 && waehler.getY() == 500)
                || (waehler.getX() == 400 && waehler.getY() == 600))) {
            return !bewegbar;
        } // Stein bei 300,500
        else if ((getSteine().get(waehler.getTempIndex()).getX() == 300
                && getSteine().get(waehler.getTempIndex()).getY() == 500)
                && ((waehler.getX() == 300 && waehler.getY() == 400)
                || (waehler.getX() == 400 && waehler.getY() == 500))) {
            return !bewegbar;
        } // Stein bei 300,400
        else if ((getSteine().get(waehler.getTempIndex()).getX() == 300
                && getSteine().get(waehler.getTempIndex()).getY() == 400)
                && ((waehler.getX() == 300 && waehler.getY() == 300)
                || (waehler.getX() == 300 && waehler.getY() == 500)
                || (waehler.getX() == 200 && waehler.getY() == 400))) {
            return !bewegbar;
        }
        return bewegbar;
    }
    public int getVerbleibendeSteine() {
        return verbleibendeSteine;
    }

    public void setVerbleibendeSteine(int verbleibendeSteine) {
        this.verbleibendeSteine -= verbleibendeSteine;
    }

    private void zaehlerFuerMuehleZuruecksetzen() {
        zaehlerFuerMuehle = 0;
    }


    public int getZug() {
        return zug;
    }

    public void setZug(int zug) {
        this.zug += zug;
    }

    public boolean isMuehle() {
        return muehle;
    }

    public void setMuehle(boolean muehle) {
        this.muehle = muehle;
    }

    public ArrayList<Stein> getSteine() {
        return steine;
    }

    public int getEntfernteSteine() {
        return entfernteSteine;
    }

    public void setEntfernteSteine(int entfernteSteine) {
        this.entfernteSteine += entfernteSteine;
    }
}

