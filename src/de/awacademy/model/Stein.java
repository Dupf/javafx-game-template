package de.awacademy.model;

public class Stein {
    private boolean weiß;
    private int hight;
    private int width;
    private int x;
    private int y;


    public Stein(boolean weiß, int x, int y) {
        this.weiß = weiß;
        hight = 15;
        width = 15;
        this.x = x;
        this.y = y;
    }


    public boolean isWeiß() {
        return weiß;
    }

    public int getHight() {
        return hight;
    }

    public int getWidth() {
        return width;
    }

    public void setHight(int hight) {
        this.hight = hight;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

}

